﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIAuthJWT.Models
{
  public class TokenConfigurations
  {
    public string Audience { get; set; } = "ExemploAudience";
    public string Issuer { get; set; } = "ExemploIssuer";
    public int Seconds { get; set; } = 60;
  }
}
